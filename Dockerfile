FROM python:3-alpine

ADD src /club-bot
RUN apk add build-base
RUN pip install -r /club-bot/requirements.txt

ENTRYPOINT [ "python3", "/club-bot/main.py" ]
