#!/usr/bin/env python3

"""

"""

# STDLIB IMPORTS
from argparse import ArgumentParser
from os import environ
from os.path import dirname, join
from sys import exit

# THIRD PARTY IMPORTS
from discord import Client


def _parse_args():
    """
    Parse the arguments passed in from the command
    line.
    """
    parser = ArgumentParser()

    parser.add_argument("-b", "--bot-channel", default="bot-commands")
    parser.add_argument("-c", "--check-in-channel", default="5-am-check-in")
    parser.add_argument("-g", "--guild-name", default="5am Club")

    return parser.parse_args()


class ClubBot(Client):
    def __init__(self, bot_channel, check_in_channel, guild_name):
        """
        Override the init method of Client.
        """
        self._bot_channel_name = bot_channel
        self._check_in_channel_name = check_in_channel
        self._guild_name = guild_name

        self.guild = None
        self.bot_channel = None
        self.check_in_channel = None

        super().__init__()

    async def on_ready(self):
        """
        Log a message when the bot is ready.
        """
        for guild in self.guilds:
            if guild.name == self._guild_name:
                self.guild = guild
                break

        if not self.guild:
            print("Couldn't find your guild.")
            exit(-1)

        for channel in self.guild.channels:
            if channel.name == self._bot_channel_name:
                self.bot_channel = channel
                continue
            elif channel.name == self._check_in_channel_name:
                self.check_in_channel = channel
                continue

        if not self.bot_channel or not self.check_in_channel:
            print("Cannot find bot channel or check in channel")
            exit(1)

        print(f"{self.user} has connected to Discord!")


    async def on_message(self, message):
        """
        Deal with a message
        """
        # Make sure we don't respond to the
        # messages by the bot.
        if message.author == self.user:
            return

        # Makes sure we're responding to messages
        # in the bot-commands channel only.
        if message.channel.id != self.bot_channel.id:
            return

        if message.content.startswith("!check-in"):
            user = f"<@{message.author.id}>"
            await self.check_in_channel.send(f"Let's study {user}!")
            await message.delete()


if __name__ == "__main__":
    args = _parse_args()

    _bot_token = environ.get("BOT_TOKEN")

    client = ClubBot(bot_channel=args.bot_channel, check_in_channel=args.check_in_channel, guild_name=args.guild_name)
    client.run(_bot_token)
